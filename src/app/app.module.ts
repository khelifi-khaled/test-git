import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestKhaledComponentComponent } from './components/test-khaled-component/test-khaled-component.component';
import { JmiComponent } from './components/jmi/jmi.component';

@NgModule({
  declarations: [
    AppComponent,
    TestKhaledComponentComponent,
    JmiComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
