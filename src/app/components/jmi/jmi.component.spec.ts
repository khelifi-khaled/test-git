import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JmiComponent } from './jmi.component';

describe('JmiComponent', () => {
  let component: JmiComponent;
  let fixture: ComponentFixture<JmiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JmiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
