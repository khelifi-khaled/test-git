import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestKhaledComponentComponent } from './test-khaled-component.component';

describe('TestKhaledComponentComponent', () => {
  let component: TestKhaledComponentComponent;
  let fixture: ComponentFixture<TestKhaledComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestKhaledComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestKhaledComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
