import { TestBed } from '@angular/core/testing';

import { KhaledResolver } from './khaled.resolver';

describe('KhaledResolver', () => {
  let resolver: KhaledResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(KhaledResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
